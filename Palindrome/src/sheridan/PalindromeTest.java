package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome() {
		assertTrue("Invalid Value for palindrome", Palindrome.isPalindrome("anna"));
	}
	@Test
	public void testIsPalindromeBoundaryIn() {
		assertTrue("Invalid Value for palindrome", Palindrome.isPalindrome("a"));
	}
	@Test
	public void testIsPalindromeBoundaryOut() {
		assertFalse("This is a palindrome", Palindrome.isPalindrome("ann"));
	}
	@Test
	public void testIsPalindromeNegative() {
		assertFalse("This is a palindrome", Palindrome.isPalindrome("abc"));
	}
}
